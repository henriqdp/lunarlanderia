import argparse
import sys
import time

from IA import LunarLanderIA
from IA.utils import load_weights

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--mode', help="Modo de execução. 'run' gera pesos aleatórios \
    caso um arquivo de pesos não tenha sido informado, renderizando a execução do ambiente. \
    'evaluate' avalia a performance dos pesos informados no arquivo, sem renderização.", required=True)
    parser.add_argument('--weights', help="Arquivo com pesos a serem carregados no início da execução do ambiente.")
    args = parser.parse_args()
    # print(args.mode)
    if args.mode == 'run':
        try:
            try:
                if args.weights:
                    weights = load_weights(args.weights)
            except IOError:
                print('Arquivo de pesos não encontrado. Gerando pesos aleatórios')
                weights = None

            a = LunarLanderIA(weights=weights)
            while True:
                a.take_action(render=True)
        except KeyboardInterrupt:
            print('\nSalvando pesos e encerrando.')
            a.save_weights()
    elif args.mode == 'evaluate':
        try:
            if not args.weights:
                print('Pesos são obrigatórios no modo evaluate.')
            else:
                try:
                    if args.weights:
                        weights = load_weights(args.weights)
                except IOError:
                    sys.exit('Arquivo de pesos não encontrado.')

                a = LunarLanderIA(weights)
                while True:
                    a.take_action(render=False)
                    if a.done:
                        time.sleep(0.5)
        except KeyboardInterrupt:
            a.save_weights()
            print('\nSalvando pesos e encerrando.')

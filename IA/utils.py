import numpy as np

from . import LunarLanderIA


def average_reward(weights=None, epochs=100):
    env = LunarLanderIA(weights)
    reward = 0
    for i in range(epochs):
        env.reset()
        while env.done is False:
            env.take_action(render=False)
        reward += env.reward
    return reward / float(epochs)


def single_run(weights=None):
    return -1 * average_reward(weights, 5)


def load_weights(filename):
    w = np.loadtxt('%s' % filename)
    return w

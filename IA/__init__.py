import time
from datetime import datetime

import numpy as np

from .lunar_lander import LunarLander


class LunarLanderIA(object):
    def __init__(self, weights=None):
        self.env = LunarLander()
        self.episode = 0
        self.current_state = self.reset()

        if weights is None:
            self.weights = np.random.rand(len(self.compute_features()))
        else:
            if type(weights) == np.ndarray:
                self.weights = weights

    def reset(self):
        self.reward = 0
        self.episode += 1
        self.done = False
        return self.env.reset()

    def take_action(self, render=False, print_reward=True):
        if self.done:
            if print_reward:
                print("Epoch %3d ended with total reward received = %.2f" % (self.episode, self.reward))
            self.current_state = self.reset()

        else:
            if render:
                self.env.render()
            state, reward, done, unused = self.env.step(self.compute_action())
            self.current_state = state
            self.reward += reward
            self.done = done
        return self.reward

    def save_weights(self):
        ts = time.time()
        ts = datetime.fromtimestamp(ts).strftime('%Y%m%d%H%M%S')
        np.savetxt("weights/%s.csv" % ts, self.weights, delimiter=',')

    def act(self, angle):
        s = self.current_state
        hover_targ = 0.55 * np.abs(s[0])
        hover_todo = (hover_targ - s[1]) * 0.5 - (s[3]) * 0.5

        if s[6] or s[7]:
            hover_todo = -(s[3]) * 0.5

        a = np.array([hover_todo * 20 - 1, angle])
        a = np.clip(a, -1, +1)
        return a

    def compute_features(self):
        """
            Função a ser completada pelo aluno e que deve retornar o vetor de
            features do sistema com base nas informações do estado do agente.

            Informações do estado estão disponíveis em self.current_state

            O estado é um vetor de oito dimensões com os seguintes valores em ordem:
                [0] posição x da nave;
                [1] posição y da nave;
                [2] componente x do vetor velocidade da nave;
                [3] componente y do vetor velocidade da nave;
                [4] ângulo da nave;
                [5] velocidade angular da nave;
                [6] variável booleana que é True caso a perna esquerda da nave esteja
                    tocando o chão e False caso contrário;
                [7] variável booleana que é True caso a perna direita da nave esteja
                    tocando o chão e False caso contrário;
        """

        # COMPLETAR
        # Exemplo de retorno:
        return [self.current_state[0],
                self.current_state[1]]
        # return None

    def compute_action(self):
        """
            Função que computará a próxima ação a ser tomada pelo agente. Ela
            deve retornar obrigatoriamente o resultado do método self.act, ou seja,
            a última linha deste método não pode ser modificada.

            Abaixo, um exemplo de implementação da função. Algoritmos de refinamento
            dos pesos devem ser inseridos aqui.
        """
        features = np.array(self.compute_features())
        action = np.sum(features * self.weights)
        return self.act(action)
